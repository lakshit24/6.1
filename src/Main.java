	class Employee {
	   private double pay;
   private String name;
	   public Employee(double Pay, String Name) {
       super();
       pay = Pay;
       name = Name;
   }
	   public double getPay() {
       return pay;
   }
	   public void setPay(double Pay) {
       pay = Pay;
   }
	   public String getName() {
       return name;
   }
	   public void setName(String Name) {
       name = Name;
   }
	   // calculates the pay based on hours
   public double calculatePay(int hours) {
       double res = 0;
       if (hours < 40) {
           res = hours * pay;
           // if hours > 40 than pay will be 1.5
       } else {
           res = 40 * pay;
           res = res + (hours - 40) * (pay * 1.5);
       }
       return res;
   }
	}
	class Manager extends Employee {
   private double bonus;
	   public Manager(double Pay, String Name, double Bonus) {
       super(Pay, Name);
       bonus = Bonus;
   }
	   public double getBonus() {
       return bonus;
           }
	   public void setBonus(double Bonus) {
       bonus = Bonus;
   }
	   public double calculatePay(int hours) {
       // calling super class method to get the paymenet and adding the bonus to it
       double pay = super.calculatePay(hours);
       pay = pay * bonus + pay;
       return pay;
   }
}
	
