public class PayrollSimulation {
   public static void main(String[] args) {
       Manager man = new Manager(100, "Simran", 0.1);
       Employee emp = new Employee(80, "Laksh");
       System.out.println("Salary : "+emp.calculatePay(50));
       System.out.println("Salary : "+man.calculatePay(50));
   }
}